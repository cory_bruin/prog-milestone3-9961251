﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_9961251
{
    class Program
    {
        static void Main(string[] args)
        {
            //2 Variables initiated to retain memory until program is closed.
            var customerNumber = 0;
            var customerInfo = new List<Tuple<int, string, string>>();
            //calls organisational method and gives the method memory variables
            corysPizza(customerNumber, customerInfo);
        }
        static void corysPizza(int customerNumber, List<Tuple<int, string, string>> customerInfo)
        {
            //lines 21-27 resets variables for defaults for every new order
            customerNumber++;
            List<Tuple<int, string, double>> menu = new List<Tuple<int, string, double>>();
            List<Tuple<int, string, string, double>> order = new List<Tuple<int, string, string, double>>();
            var orderStage = "pizza";
            var response = "";
            //Calls PizzaList method in Pizza class and loads menu onto list.
            Pizza.PizzaList(menu);
            Console.WriteLine("       Welcome to Corys Pizzas        ");
            //Calls PizzaMenu method in Pizza class and displays menu to console
            Pizza.PizzaMenu(menu, orderStage);
            //Calls Order method in Pizza class which handles all ordering input
            Pizza.Order(menu, order, orderStage);
            //while loop that doesn't break until user is finished ordering pizza
            var loop = 0;
            while (loop == 0)
            {
                Console.WriteLine("Would you like to order another Pizza.");
                response = Console.ReadLine();
                response = Response(response);
                if (response == "Yes")
                {
                    Pizza.PizzaMenu(menu, orderStage);
                    Pizza.Order(menu, order, orderStage);
                }
                else { loop++; }
            }
            //checks if user would like to order drinks, if yes, loads the menu List with values from drinksList method in Pizza class, displays drinks menu and initiates ordering input
            Console.WriteLine("Would you like to order any drinks?");
            response = Console.ReadLine();
            response = Response(response);
            if (response == "Yes")
            {
                menu = new List<Tuple<int, string, double>>();
                orderStage = "drinks";
                Pizza.drinksList(menu);
                Pizza.PizzaMenu(menu, orderStage);
                Pizza.Order(menu, order, orderStage);
                loop = 0;
            }
            else { loop = 1; }
            //while loop that doesn't break until user is finished ordering drinks
            while (loop == 0)
            {
                Console.WriteLine("Would you like to order another drink?");
                response = Console.ReadLine();
                response = Response(response);
                if (response == "Yes")
                {
                    Pizza.PizzaMenu(menu, orderStage);
                    Pizza.Order(menu, order, orderStage);
                }
                else { loop++; }
            }
            //Gets name for order, displays order and gets program ready for payment
            Console.WriteLine("What is your name for the order?");
            var name = Console.ReadLine();
            Console.WriteLine();
            Console.WriteLine("Your Order:");
            var orderCount = order.Count();
            var orderTotal = 0.00;
            var x = 0;
            while (x < orderCount)
            {
                orderTotal = orderTotal + order[x].Item4;
                Console.WriteLine($"{order[x].Item3} {order[x].Item2}");
                x++;
            }
            Console.WriteLine($"Order total: ${orderTotal}");
            //sends the orderTotal to the payment method, where program ensures correct amount is paid
            Payment(orderTotal);
            Console.WriteLine("Thank you for your payment");
            customerInfo = Customer.customerInfo(order, customerNumber, customerInfo, name);
            //Once intial order is placed, menu is called allowing user to check orders in system or place new order
            Menu(customerNumber, customerInfo);
        }
        static void Menu(int customerNumber, List<Tuple<int, string, string>> customerInfo)
        {
            //simple menu allowing users to check orders in system or place another order
            Console.WriteLine();
            Console.WriteLine("Press 1 to see all orders");
            Console.WriteLine("Press 2 to place another order");
            Console.WriteLine("Anything else to exit");
            var input = Console.ReadLine();
            switch (input)
            {
                case "1":
                    Console.Clear();
                    var count = customerInfo.Count();
                    var i = 0;
                    var firstRun = 0;
                    var outputTest = customerInfo[i].Item1;
                    while (count > i)
                    {
                        if (firstRun == 0)
                        {
                            Console.WriteLine($"Customer Number: {customerInfo[i].Item1} | Customer Name: {customerInfo[i].Item2}");
                            firstRun = 1;
                        }
                        if (customerInfo[i].Item1 == outputTest)
                        {
                            Console.WriteLine($"Item: {customerInfo[i].Item3}");
                            i++;
                            outputTest = customerInfo[i - 1].Item1;
                        }
                        else
                        {
                            Console.WriteLine($"Customer Number: {customerInfo[i].Item1} | Customer Name: {customerInfo[i].Item2}");
                            Console.WriteLine($"Item: {customerInfo[i].Item3}");
                            i++;
                            outputTest = customerInfo[i - 1].Item1;
                        }
                    }
                    Menu(customerNumber, customerInfo);
                    break;
                case "2":
                    Console.Clear();
                    corysPizza(customerNumber, customerInfo);
                    break;
                case null:
                    Environment.Exit(0);
                    break;
                default:
                    Environment.Exit(0);
                    break;
            }
        }
        static string Response(string response)
        {
            //minimising potential for program to break
            if (response == "yes" || response == "Yes" || response == "y" || response == "Y" || response == "YES") { response = "Yes"; }
            return response;
        }
        static double Payment(double orderTotal)
        {
            //Payment method ensure customer pays the right total and informing employee how much change
            Console.WriteLine("Please enter in the amount of money you're giving the cashier");
            var input = Console.ReadLine();
            var money = 0.0;
            bool isNum = double.TryParse(input, out money);
            if (money >= orderTotal)
            {
                Console.Clear();
                Console.WriteLine($"Money Given: ${money}, Order Total: ${orderTotal}. Change: ${money - orderTotal}");
            }
            else { Console.WriteLine("Insufficient money given to cashier; try again"); Payment(orderTotal); }
            return orderTotal;
        }
    }
    class Pizza
    {
        //Pizza Class contains all information about Pizza/Drinks
        public static void PizzaMenu(List<Tuple<int, string, double>> menu, string orderStage)
        {
            //Main Menu that checks if user is ordering Pizza or Drinks then inputs correct information onto menu
            Console.WriteLine("**************************************");
            Console.WriteLine("*                Menu                *");
            if (orderStage == "pizza")
            {
                Console.WriteLine("*     Pizza - SML | MED | LARGE      *");
            }
            if (orderStage == "drinks")
            {
                Console.WriteLine("*             Drinks List            *");
            }
            Console.WriteLine("*                                    *");
            //while loops to display correct output for pizza/drinks depending what stage user is in
            var i = 0;
            var count = menu.Count();
            if (orderStage == "pizza")
            {
                while (i < count)
                {
                    var linelength = ($"{menu[i].Item1}. {menu[i].Item2} - {menu[i].Item3} | ${menu[i].Item3 + 1.5} | ${menu[i].Item3 + 2.5}");
                    Console.Write(new string(' ', (37 - linelength.Length) / 2));
                    Console.WriteLine($"{menu[i].Item1}. {menu[i].Item2} - ${menu[i].Item3} | ${menu[i].Item3 + 1.5} | ${menu[i].Item3 + 2.5}");
                    i++;
                }
            }
            if (orderStage == "drinks")
            {
                while (i < count)
                {
                    var linelength = ($"{menu[i].Item1}. {menu[i].Item2} - {menu[i].Item3}");
                    Console.Write(new string(' ', (37 - linelength.Length) / 2));
                    Console.WriteLine($"{menu[i].Item1}. {menu[i].Item2} - ${menu[i].Item3}");
                    i++;
                }
            }
            Console.WriteLine("*                                    *");
            Console.WriteLine("*                                    *");
            Console.WriteLine("**************************************");
        }
        public static List<Tuple<int, string, string, double>> Order(List<Tuple<int, string, double>> menu, List<Tuple<int, string, string, double>> order, string orderStage)
        {
            //method where all ordering is handled, initialise variables
            Console.WriteLine("    What would you like to order?     ");
            var item = Console.ReadLine();
            item = item.ToUpper();
            int itemNum = 0;
            bool isNum = int.TryParse(item, out itemNum);
            var count = menu.Count();
            //calling the PizzaType method - checks if user has inputted name or pizza or the number and returns a general input the program can work with.
            item = PizzaType(item, itemNum, menu);
            var i = 0;
            var size = "";
            var pizzaFound = 0;
            //if user is ordering pizza input information into order list in this format
            if (orderStage == "pizza")
            {
                while (count > i)
                {
                    if (menu[i].Item2 == item)
                    {
                        Console.WriteLine("What size pizza would you like? Small, Medium or Large");
                        Console.WriteLine($"{menu[i].Item2} - Small ${menu[i].Item3} | Medium ${menu[i].Item3 + 1.5} | Large ${menu[i].Item3 + 2.5}");
                        size = Console.ReadLine();
                        size = PizzaSize(size);
                        if (size == "Small") { order.Add(Tuple.Create(i + 1, menu[i].Item2, size, menu[i].Item3)); pizzaFound = 1; }
                        if (size == "Medium") { order.Add(Tuple.Create(i + 1, menu[i].Item2, size, menu[i].Item3 + 1.5)); pizzaFound = 1; }
                        if (size == "Large") { order.Add(Tuple.Create(i + 1, menu[i].Item2, size, menu[i].Item3 + 2.5)); pizzaFound = 1; }
                    }
                    i++;
                }
            }
            //if user is ordering drinks input information into order list in this format
            if (orderStage == "drinks")
            {
                while (count > i)
                {
                    if (menu[i].Item2 == item)
                    {
                        order.Add(Tuple.Create(i + 1, menu[i].Item2, "Regular", menu[i].Item3)); pizzaFound = 1;
                    }
                    i++;
                }
            }
            //if statement to ensure user enters valid option
            if (pizzaFound == 0) { Console.WriteLine("Invalid Selection"); Order(menu, order, orderStage); }
            //if successful console clears screen - shows user their current order and order total, returns value to menu where user can reenter this method to add another pizza
            Console.Clear();
            Console.WriteLine($"{item} added.");
            var orderCount = order.Count();
            var x = 0;
            var orderTotal = 0.0;
            Console.WriteLine();
            Console.WriteLine("Current Order:");
            while (x < orderCount)
            {
                orderTotal = orderTotal + order[x].Item4;
                Console.WriteLine($"{order[x].Item3} {order[x].Item2}");
                x++;
            }
            Console.WriteLine($"Order total: ${orderTotal}");
            Console.WriteLine();
            return order;
        }
        static string PizzaType(string item, int itemNum, List<Tuple<int, string, double>> menu)
        {
            //method called from Order method - checks position 1 of menu list and matches with user input if they inputted a number - returns position 2 in list which is the name of the pizza
            var i = 0;
            var count = menu.Count();
            while (count > i)
            {
                if (menu[i].Item1 == itemNum)
                {
                    item = menu[i].Item2;
                    return item;
                }
                i++;
            }
            return item;
        }
        public static List<Tuple<int, string, double>> PizzaList(List<Tuple<int, string, double>> menu)
        {
            //method where the pizza menu is loaded - easy to add/remove pizzas or edit prices
            menu.Add(Tuple.Create(1, "CHEESE", 7.00));
            menu.Add(Tuple.Create(2, "HAWAIIAN", 8.50));
            menu.Add(Tuple.Create(3, "SUPREME", 9.50));
            menu.Add(Tuple.Create(4, "ITALIANO", 9.50));
            menu.Add(Tuple.Create(5, "VEGETARIAN", 9.50));
            menu.Add(Tuple.Create(6, "MEATLOVERS", 10.50));
            menu.Add(Tuple.Create(7, "BBQ CHICKEN", 10.50));
            return menu;
        }
        public static List<Tuple<int, string, double>> drinksList(List<Tuple<int, string, double>> drinks)
        {
            //method where the drinks menu is loaded - easy to add/remove drinks or edit prices
            drinks.Add(Tuple.Create(1, "COKE", 4.50));
            drinks.Add(Tuple.Create(2, "PEPSI", 4.50));
            drinks.Add(Tuple.Create(3, "BEER", 7.00));
            drinks.Add(Tuple.Create(4, "WINE", 7.00));
            return drinks;
        }
        static string PizzaSize(string size)
        {
            //minimising risk - allowing users to enter in the pizza size in various ways
            if (size == "small" || size == "Small" || size == "s" || size == "S" || size == "SMALL") { size = "Small"; }
            if (size == "medium" || size == "Medium" || size == "m" || size == "M" || size == "MEDIUM") { size = "Medium"; }
            if (size == "large" || size == "Large" || size == "l" || size == "L" || size == "LARGE") { size = "Large"; }
            return size;
        }
    }
    class Customer
    {
        public static List<Tuple<int, string, string>> customerInfo(List<Tuple<int, string, string, double>> order, int customerNumber, List<Tuple<int, string, string>> customerInfo, string name)
        {
            //Customer Class where the Customers order number, name and items purchased are added to a list that can be called from the submenu allowing temp memory for duration of program
            var count = order.Count();
            var i = 0;
            while (count > i)
            {
                customerInfo.Add(Tuple.Create(customerNumber, name, order[i].Item2));
                i++;
            }
            return customerInfo;
        }
    }
}
